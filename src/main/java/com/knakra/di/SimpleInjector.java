package com.knakra.di;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.knakra.di.exceptions.CircularDependencyException;
import com.knakra.di.exceptions.ClassInstantiationException;
import com.knakra.di.exceptions.NoInjectableConstructorsFoundException;

/**
 * SimpleInjector - A lightweight dependency injection container.
 *
 * <p>
 * This class provides basic dependency injection capabilities by allowing
 * interfaces to be mapped to their implementations. When an instance of a class
 * (or interface)
 * is requested, the injector will either return a cached instance or create a
 * new one. If a
 * new instance is required, the injector will recursively instantiate any
 * dependencies needed
 * by analyzing the constructors of the implementation class.
 *
 * <p>
 * Important: Constructors intended for dependency injection must be annotated
 * with {@link Injectable}.
 * This ensures explicitness in indicating which constructors should be managed
 * by the DI system.
 *
 * <p>
 * Usage:
 * <ol>
 * <li>Create an instance of the injector using the default constructor
 * {@link #SimpleInjector()}.
 * <li>Map an interface to its implementation using the
 * {@link #map(Class, Class)} method.
 * <li>Retrieve instances using the {@link #getInstance(Class)} method.
 * </ol>
 *
 * Features:
 * <ul>
 * <li>Singleton scope: Once an instance is created, it's cached and reused for
 * subsequent requests.
 * <li>Constructor-based injection: The injector analyzes constructors to
 * automatically and recursively
 * instantiate required dependencies.
 * <li>Visibility-Independent Instantiation: The injector is capable of
 * instantiating classes regardless of their constructor visibility (public,
 * protected, package-private, or private). This flexibility supports advanced
 * encapsulation patterns by allowing the injector to manage instances of
 * classes without requiring their constructors to be exposed publicly.
 * </ul>
 * <p>
 * Note: This injector does not support multiple constructors with parameters
 * for the same class.
 * In case multiple such constructors are present, behavior is undetermined.
 * Ensure that only one
 * constructor is annotated with {@link Injectable} and requires parameter-based
 * injection, or use
 * a no-args constructor.
 *
 * <p>
 * Example:
 *
 * <pre>
 * {@code
 * SimpleInjector injector = new SimpleInjector();
 * injector.map(InterfaceType.class, ImplementationType.class);
 * InterfaceType instance = injector.getInstance(InterfaceType.class);
 * }
 * </pre>
 *
 * @author Knakra
 * @version 1.0
 */

public class SimpleInjector {

    private final ThreadLocal<List<Class<?>>> creationStack = ThreadLocal.withInitial(ArrayList::new);

    private Map<Class<?>, Object> instances = new HashMap<>();
    private Map<Class<?>, Class<?>> interfaceToImplMapping = new HashMap<>();

    /**
     * Maps an interface to its implementation.
     *
     * @param interfaceType The interface type
     * @param implType      The implementation type
     */
    public void map(Class<?> interfaceType, Class<?> implType) {
        interfaceToImplMapping.put(interfaceType, implType);
    }

    /**
     * Returns an instance of the given class.
     *
     * @param <T>   The type of the instance
     * @param clazz The class to instantiate
     * @return An instance of the given class
     */
    public <T> T getInstance(Class<T> clazz) {
        T instance = clazz.cast(instances.get(clazz));
        if (instance == null) {
            instance = createInstance(clazz);
        }
        return instance;
    }

    /**
     * Creates an instance of the given class.
     *
     * @param <T>  The type of the instance
     * @param type The class to instantiate
     * @return An instance of the given class
     */
    private <T> T createInstance(Class<T> type) {

        // Return instance if it already exists
        if (instances.containsKey(type)) {
            return type.cast(instances.get(type));
        }

        // Check for circular dependencies
        List<Class<?>> stack = creationStack.get();
        if (stack.contains(type)) {
            throw new CircularDependencyException("Circular dependency detected: " + stack + " -> " + type);
        }
        // Get the implementation type
        Class<?> implType = interfaceToImplMapping.getOrDefault(type, type);

        try {
            stack.add(type);
            @SuppressWarnings("unchecked") // We know that implType is of type T
            T instance = instantiate((Class<T>) implType);
            instances.put(type, instance);
            return instance;
        } catch (ReflectiveOperationException e) {
            throw new ClassInstantiationException("Failed to instantiate class: " + implType.getName(), e);
        } finally {
            stack.remove(type);

            // Remove the stack if it's empty to avoid memory leaks (java:S5164)
            if(stack.isEmpty()) {
                creationStack.remove();
            }
        }
    }

    @SuppressWarnings("unchecked") // We know that the implementation type is of type T
    private <T> T instantiate(Class<T> implementation) throws ReflectiveOperationException {
        Constructor<?>[] constructors = implementation.getDeclaredConstructors();

        for (Constructor<?> constructor : constructors) {
            if (constructor.isAnnotationPresent(Injectable.class)) {

                // Make the constructor accessible if it's not public
                if (!Modifier.isPublic(constructor.getModifiers())) {
                    constructor.setAccessible(true);
                }

                // If the constructor has no parameters, simply instantiate the class
                // Otherwise, recursively instantiate dependencies
                if (constructor.getParameterCount() == 0) {
                    return (T) constructor.newInstance();
                } else {
                    List<Object> parameters = new ArrayList<>();
                    for (Class<?> paramType : constructor.getParameterTypes()) {
                        Object paramInstance = getInstance(paramType); // Recursively instantiate dependencies
                        parameters.add(paramInstance);
                    }
                    return (T) constructor.newInstance(parameters.toArray());
                }
            }
        }

        // If we reached this point, no @Injectable constructor was found
        throw new NoInjectableConstructorsFoundException(
                "Class: " + implementation.getName() + " does not have an " + Injectable.class.getName()
                        + " constructor");
    }
}
