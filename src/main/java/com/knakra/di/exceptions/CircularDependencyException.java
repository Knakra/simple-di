package com.knakra.di.exceptions;

/**
 * CircularDependencyException - Exception thrown when a circular dependency is
 * detected.
 */
public class CircularDependencyException extends RuntimeException {

    public CircularDependencyException(String message) {
        super(message);
    }

}
