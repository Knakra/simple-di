package com.knakra.di.exceptions;

/**
 * ClassInstantiationException - Exception thrown when an instance of a class cannot be created.
 */
public class ClassInstantiationException extends RuntimeException {

    public ClassInstantiationException(String message) {
        super(message);
    }

    public ClassInstantiationException(String message, ReflectiveOperationException cause) {
        super(message, cause);
    }

    public ClassInstantiationException(ReflectiveOperationException cause) {
        super(cause);
    }
}
