package com.knakra.di.exceptions;

/**
 * NoInjectableConstructorsFoundException - Exception thrown when no injectable
 * constructors are found in a class.
 */
public class NoInjectableConstructorsFoundException extends RuntimeException {

    public NoInjectableConstructorsFoundException(String message) {
        super(message);
    }

}
