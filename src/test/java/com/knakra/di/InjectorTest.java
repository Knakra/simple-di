package com.knakra.di;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import com.knakra.di.exceptions.CircularDependencyException;
import com.knakra.di.exceptions.NoInjectableConstructorsFoundException;
import com.knakra.di.testclasses.CircularDependencyTestClasses;
import com.knakra.di.testclasses.InjectableAnnotationMissingTestClasses;
import com.knakra.di.testclasses.TestClasses;

class InjectorTest {

    @Test
    void injectorTest() {

        // Create a new instance of SimpleInjector
        SimpleInjector injector = new SimpleInjector();

        // Map the interface to the implementation
        injector.map(TestClasses.ClassA.class, TestClasses.ClassAImpl.class);
        injector.map(TestClasses.ClassB.class, TestClasses.ClassBImpl.class);

        // Check if the instance is of the expected class
        assertEquals(TestClasses.ClassAImpl.class, injector.getInstance(TestClasses.ClassA.class).getClass());
    }

    @Test
    void circularDependencyTest() {
        // Create a new instance of SimpleInjector
        SimpleInjector injector = new SimpleInjector();

        // Map the interfaces to the implementations
        injector.map(CircularDependencyTestClasses.ClassA.class, CircularDependencyTestClasses.ClassAImpl.class);
        injector.map(CircularDependencyTestClasses.ClassB.class, CircularDependencyTestClasses.ClassBImpl.class);

        assertThrows(CircularDependencyException.class, () -> {
            injector.getInstance(CircularDependencyTestClasses.ClassA.class);
        });
    }

    @Test
    void injectableAnnotationMissingTest() {
        // Create a new instance of SimpleInjector
        SimpleInjector injector = new SimpleInjector();

        // Map the interfaces to the implementations
        injector.map(InjectableAnnotationMissingTestClasses.ClassA.class,
                InjectableAnnotationMissingTestClasses.ClassAImpl.class);
        injector.map(InjectableAnnotationMissingTestClasses.ClassB.class,
                InjectableAnnotationMissingTestClasses.ClassBImpl.class);

        assertThrows(NoInjectableConstructorsFoundException.class, () -> {
            injector.getInstance(InjectableAnnotationMissingTestClasses.ClassA.class);
        });
    }

}
