package com.knakra.di.testclasses;

import com.knakra.di.Injectable;

public class InjectableAnnotationMissingTestClasses {

    // Interface for ClassA
    public interface ClassA {
        void action();
    }

    // Interface for ClassB
    public interface ClassB {
        void perform();
    }

    // Implementation of ClassA
    public static class ClassAImpl implements ClassA {
        private final ClassB classB;

        @Injectable
        public ClassAImpl(ClassB classB) {
            this.classB = classB;
            System.out.println("ClassAImpl instantiated");
        }

        @Override
        public void action() {
            System.out.println("ClassA action triggered");
            classB.perform(); // Triggers ClassB
        }
    }

    // Implementation of ClassB
    public static class ClassBImpl implements ClassB {
        private final ClassA classA;

        public ClassBImpl(ClassA classA) {
            this.classA = classA;
            System.out.println("ClassBImpl instantiated");
        }

        @Override
        public void perform() {
            System.out.println("ClassB performing");
            classA.action(); // Triggers ClassA
        }
    }
}