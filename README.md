# Simple DI

## Description

Simple Dependency Injection for Java.

## Pre-requisites

- Maven >=3.8.5
- Java >=11

## Installation

First install using maven by running:

```bash
mvn clean install
```

Then add the following dependency to your project's `pom.xml`:

```xml
<dependency>
    <groupId>com.knakra.di</groupId>
    <artifactId>di</artifactId>
    <version>1.2.0-SNAPSHOT</version>
</dependency>
```

## Usage

```java
// Create a new instance of SimpleInjector
SimpleInjector injector = new SimpleInjector();

// (Optional) Interface to Implementation mapping (when requesting instances for given interface)
injector.map(ClassA.class, ClassAImpl.class);
injector.map(ClassB.class, ClassBImpl.class);

// The injector will create the instance of ClassAImpl
// When ClassAImpl needs an instance of ClassB, the injector will handle the DI process
injector.getInstance(ClassA.class).methodInClassA();
```

See tests for more examples.
